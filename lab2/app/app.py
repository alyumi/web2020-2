from flask import Flask, render_template, request, make_response
import operator as op

app = Flask(__name__)
application = app

operations = ['+', '-', '*', '/']
operations_functions = { '+': op.add, '-': op.sub, '*': op.mul, '/': op.truediv }

allowed_sym = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '.', '-', '(', ')', '+']
dij = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

@app.route('/')
def index():
    return render_template('index.html')
    
@app.route('/args')
def args():
    return render_template('args.html')

@app.route('/headers')
def headers():
    return render_template('headers.html')
    
@app.route('/cookies')
def cookies():
    resp = make_response(render_template('cookies.html'))
    if 'username' in request.cookies:
        resp.set_cookie('username', 'some name', expires=0)
    else:
        resp.set_cookie('username', 'some name')
    
    return resp

@app.route('/form', methods=['GET', 'POST'])
def form():
    return render_template('form.html')

@app.route('/calc')
def calc():
    result = None
    error_msg = None
    try:
        op1 = float(request.args.get('operand1'))
        op2 = float(request.args.get('operand2'))
        f = operations_functions[request.args.get('operation')]
        result = f(op1, op2)
    except ValueError:
        error_msg = 'Пожалуйста, вводите только числа.'
    except ZeroDivisionError:
        error_msg = 'На ноль делить нельзя.'
    except KeyError:
        error_msg = 'Недопустимая операция.'
    except Exception as e:
        error_msg: 'What happend?'

    return render_template('calc.html', operations=operations, result=result, error_msg=error_msg)


@app.route('/er')
def er():
    error_msg = None
    conf_msg = None
    formatted_number = None
    number = request.args.get('number')
    er_flag = 0
    count = 0
    number_stack = []

    for i in range(len(number)):
        if (number[i] not in allowed_sym):
            er_flag = 1
            break
        elif (number[i] in dij):
            count = count + 1
            number_stack.append(number[i])

    if er_flag == 1:
        error_msg = "Недопустимый ввод. В номере телефона встречаются недопустимые символы."
    elif count > 11 or count < 10:
        error_msg = "Недопустимый ввод. Неверное количество цифр."
    else:
        conf_msg = "Введенный номер корректен."
        if len(number_stack) == 11:
            formatted_number = '8-' + number_stack[1] + number_stack[2] + number_stack[3] + '-' + number_stack[4] + number_stack[5] + number_stack[6] + '-' + number_stack[7] + number_stack[8] + '-' + number_stack[9] + number_stack[10]
        elif len(number_stack) == 10:
            formatted_number = '8-' + number_stack[0] + number_stack[1] + number_stack[2] + '-' + number_stack[3] + number_stack[4] + number_stack[5] + '-' + number_stack[6] + number_stack[7] + '-' + number_stack[8] + number_stack[9]

    return render_template('er.html', number=number, error_msg=error_msg, conf_msg=conf_msg, formatted_number=formatted_number)