from flask import Flask, render_template, request, session, redirect, url_for, flash
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required
from mysql_db import MySQL
import mysql.connector as connector

login_manager = LoginManager()

app = Flask(__name__)
application = app

app.config.from_pyfile('config.py')

mysql = MySQL(app)

login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.login_message = 'Для доступа к данной странице необходимо пройти процедуру аутентификации.'
login_manager.login_message_category = 'warning'


class User(UserMixin):
    def __init__(self, user_id, login):
        super().__init__()
        self.id = user_id
        self.login = login

@login_manager.user_loader
def load_user(user_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM users WHERE id = %s;', (user_id,))
    db_user = cursor.fetchone()
    cursor.close()
    if db_user:
        return User(user_id=db_user.id, login=db_user.login)
    return None

def load_genres():
    query = '''
        SELECT DISTINCT films.id AS id, g.g_name AS name FROM films JOIN
        (SELECT films_genres.film_id AS f_id, genres.name AS g_name FROM films_genres JOIN genres ON films_genres.genre_id = genres.id) AS g
        ON films.id = g.f_id
        ORDER BY films.id
    '''

    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute(query)
    genres = cursor.fetchall()
    cursor.close()
    g_new = []
    temp = ''

    for i in range(len(genres)):
        if genres[i].id == genres[i - 1].id:
            temp = temp + ', ' + genres[i].name
        else:
            if temp != '':
                g_new.append({genres[i-1].id : temp[2::]})
                temp = ''
            temp = temp + ', ' + genres[i].name
    g_new.append({genres[len(genres)-1].id : temp[2::]})    

    return g_new
    

def load_films():
    query='''
        SELECT films_genres.film_id AS id, genres.name AS name FROM films_genres JOIN genres ON films_genres.genre_id = genres.id
    '''
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM films;')
    films = cursor.fetchall()
    cursor.close()
    return films

def count_reviews():
    query='''
        SELECT films.id AS id, count(reviews.film_id) as ccc FROM films LEFT JOIN reviews ON films.id = reviews.film_id
        GROUP BY films.id
        ORDER BY films.id
    '''
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute(query)
    review_count = cursor.fetchall()
    cursor.close()
    r_new = []
    for rev in review_count:
        r_new.append({rev.id : rev.ccc})
    return r_new

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        remember_me = request.form.get('remember_me') == 'on'
        if login and password:
            cursor = mysql.connection.cursor(named_tuple=True)
            cursor.execute('SELECT * FROM users WHERE login = %s AND pwd_hash = MD5(%s);', (login, password))
            db_user = cursor.fetchone()
            cursor.close()
            if db_user:
                user = User(user_id=db_user.id, login=db_user.login)
                login_user(user, remember=remember_me)

                flash('Вы успешно аутентифицированны.', 'success')

                next = request.args.get('next')

                return redirect(next or url_for('index'))
        flash('Введены неверные логин и/или пароль.', 'danger')
    return render_template('login.html')

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/film')
def film():
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT users.*, roles.name AS role_name FROM users LEFT OUTER JOIN roles ON users.role_id = roles.id;')
    films = cursor.fetchall()
    cursor.close()
    return render_template('film/index.html', films=load_films(), genres=load_genres(), rev_c=count_reviews())

@app.route('/film/<int:film_id>')
def show(film_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM films WHERE id=%s', (film_id, ))
    film = cursor.fetchone()

    query = '''
        SELECT reviews.rating AS rating, reviews.description AS description, 
        users.login AS login, users.first_name AS fn, users.last_name AS ln, users.middle_name AS mn
        FROM reviews JOIN users ON reviews.user_id = users.id WHERE reviews.film_id=%s
        '''

    cursor.execute(query, (film_id, ))
    reviews = cursor.fetchall()

    cursor.execute('SELECT * FROM posters WHERE film_id=%s', (film_id, ))
    poster = cursor.fetchone()

    poster_path = '../static/img/' + poster.name + '.' + poster.mime_type

    cursor.close()

    genres = load_genres()[film_id - 1].get(film_id)    

    return render_template('film/show.html', film=film, reviews=reviews, poster=poster_path, genres=genres)

@app.route('/film/new')
@login_required
def new():
    return render_template('film/new.html', film={}, genre=load_genres())

@app.route('/film/create', methods=['POST'])
@login_required
def create():
    name = request.form.get('filmName') or None
    desc = request.form.get('desc') or None
    prod_year = request.form.get('prod_year') or None
    country = request.form.get('country') or None
    director = request.form.get('director') or None
    scenarist = request.form.get('scenarist') or None
    actors = request.form.get('actors') or None
    length = request.form.get('length') or None
    genre_id = request.form.get('genre_id') or None

    query='''
        INSERT INTO films (name, description, prod_year, country, director, scenarist, actors, length)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
    '''
    cursor = mysql.connection.cursor(named_tuple=True)
    try:
        cursor.execute(query, (name, desc, prod_year, country, director, scenarist, actors, length))
    except connector.errors.DatabaseError as err:
        flash('Введены некорректные данные. Ошибка сохранения', 'danger')
        flash(err, 'danger')
        film = {
            'name' : name,
            'desc' : desc,
            'prod_year' : prod_year,
            'country' : country,
            'director' : director,
            'scenarist' : scenarist,
            'actors' : actors,
            'length' : length
        }
        return render_template('film/new.html', film=film, genres=load_genres())
    mysql.connection.commit()
    cursor.close()
    flash(f'Фильм {name} был успешно добавлен.', 'success')
    return redirect(url_for('film'))

@app.route('/film/<int:film_id>/delete', methods=['POST'])
@login_required
def delete(film_id):
    with mysql.connection.cursor(named_tuple=True) as cursor:
        try:
            cursor.execute('DELETE FROM films WHERE id=%s;', (film_id, ))
        except connector.errors.DatabaseError as err:
            flash('Не удалось удалить запись films.', 'danger')
            return redirect(url_for('film'))
        try:
            cursor.execute('DELETE FROM posters WHERE film_id=%s;', (film_id, ))
        except connector.errors.DatabaseError as err:
            flash('Не удалось удалить запись posters.', 'danger')
            return redirect(url_for('film'))
        try:
            cursor.execute('DELETE FROM reviews WHERE film_id=%s;', (film_id, ))
        except connector.errors.DatabaseError as err:
            flash('Не удалось удалить запись reviews.', 'danger')
            return redirect(url_for('film'))
        mysql.connection.commit()
        flash('Запись была успешно удалена', 'success')
    return redirect(url_for('film'))
