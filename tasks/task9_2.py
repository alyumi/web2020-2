trunk_mode_template = [
    "switchport mode trunk", "switchport trunk native vlan 999",
    "switchport trunk allowed vlan"
]

trunk_config = {
    "FastEthernet0/1": [10, 20, 30],
    "FastEthernet0/2": [11, 30],
    "FastEthernet0/4": [17]
}

def generate_trunk_config(intf_vlan_mapping, trunk_template):
    for elem in intf_vlan_mapping:
        print("interface", elem)
        for element in trunk_template:
            if element.split()[-2] == "allowed":
                print(element, end=' ')
                for text in intf_vlan_mapping.get(elem):
                    print(text, end=',')
                print()
            else:
                print(element)


def main():
    generate_trunk_config(trunk_config, trunk_mode_template)

if __name__ == "__main__":
    main()