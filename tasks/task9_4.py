from sys import argv

filename = argv[1]

ignore = ["duplex", "alias", "Current configuration"]


def ignore_command(command, ignore):
    """
    Функция проверяет содержится ли в команде слово из списка ignore.

    command - строка. Команда, которую надо проверить
    ignore - список. Список слов

    Возвращает
    * True, если в команде содержится слово из списка ignore
    * False - если нет
    """
    return any(word in command for word in ignore)

def convert_config_to_dict(config_filename):
    file = open(config_filename, 'r')
    config_dict = dict()
    for elem in file:
        if ignore_command(elem, ignore) == False and elem[0] != '!' and elem[0] != '\n':
            if elem.split()[0] == 'no' or elem.split()[0] == 'ip' :
                config_dict.update({str(elem.split()[0]) + str(elem.split()[1]): elem.split()[2::]})
            else:
                config_dict.update({elem.split()[0]: elem.split()[1::]})


    file.close()
    return config_dict

def main():
    print(convert_config_to_dict(filename))

if __name__ == '__main__':
    main()