# Здесь необходимо вписывать номер задания в функции main
# Для того чтобы посмотреть задание


def task4_1():
    nat = 'ip nat inside source list ACL interface FastEthernet0/1 overload'

    nit = nat.replace('Fast', 'Gigabit')

    print(nit)


def task4_2():
    mac = 'AAAA:BBBB:CCCC:DDDD'

    mac_dot = mac.replace(':', '.')

    print(mac_dot)


def task4_3():
    config = 'switchport trunk allowed vlan 1,3,10,20,30,100'

    vlans = config[config.find('vlan') + 5::].split(',')
    print(vlans)


def task4_4():
    vlans = [10, 20, 30, 1, 2, 100, 10, 30, 3, 4, 10]
    vv = set(vlans)

    otv = list(vv)
    otv.sort()

    print(otv)


def task4_5():
    command1 = "switchport trunk allowed vlan 1,2,3,5,8"
    command2 = "switchport trunk allowed vlan 1,3,8,9"

    vlans1 = command1[command1.find('vlan') + 5::].split(',')
    vlans2 = command2[command2.find('vlan') + 5::].split(',')

    vv1 = set(vlans1)
    vv2 = set(vlans2)
    print(sorted(vv1 & vv2))


def task4_6():
    ospf_route = "       10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0"

    data = {}

    data.update({'Prefix': ospf_route[ospf_route.find('10'): ospf_route.find('[') - 1]})
    data.update({'AD/Metric': ospf_route[ospf_route.find('[') + 1: ospf_route.find(']')]})
    data.update({'Next-hop': ospf_route[ospf_route.find('via') + 4: ospf_route.find(',')]})
    data.update({'Last update': ospf_route[ospf_route.find(',') + 2: ospf_route.rfind(',')]})
    data.update({'Outbound Interface': ospf_route[ospf_route.rfind(',') + 2::]})

    print(data)


def task4_7():
    bmac = 0b101010101010101010111011101110111100110011001100
    mac = "AAAA:BBBB:CCCC"

    print(bin(int(mac.replace(':', ''), 16)))


def task4_8():
    ip = input()

    ipp = ip.split('.')

    print('{:10}  {:10}  {:10}  {:10}'.format(ipp[0], ipp[1], ipp[2], ipp[3]))
    print('{:010b}  {:010b}  {:010b}  {:010b}'.format(int(ipp[0]), int(ipp[1]), int(ipp[2]), int(ipp[3])))


def task5_1():
    london_co = {
        "r1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.1"
        },
        "r2": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.2"
        },
        "sw1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "3850",
            "ios": "3.6.XE",
            "ip": "10.255.0.101",
            "vlans": "10,20,30",
            "routing": True
        }
    }

    print('Input device name: ')
    device = input()

    print(london_co[device])


def task5_1a():
    london_co = {
        "r1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.1"
        },
        "r2": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.2"
        },
        "sw1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "3850",
            "ios": "3.6.XE",
            "ip": "10.255.0.101",
            "vlans": "10,20,30",
            "routing": True
        }
    }

    print('Input device name: ')
    device = input()
    print('Input device param: ')
    param = input()

    print(london_co[device][param])


def task5_1b():
    london_co = {
        "r1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.1"
        },
        "r2": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.2"
        },
        "sw1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "3850",
            "ios": "3.6.XE",
            "ip": "10.255.0.101",
            "vlans": "10,20,30",
            "routing": True
        }
    }

    print('Input device name: ')
    device = input()

    par = ', '.join(london_co[device].keys())

    print('Input device param ({}): '.format(par))
    param = input()

    print(london_co[device][param])


def task5_1c():
    london_co = {
        "r1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.1"
        },
        "r2": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.2"
        },
        "sw1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "3850",
            "ios": "3.6.XE",
            "ip": "10.255.0.101",
            "vlans": "10,20,30",
            "routing": True
        }
    }

    print('Input device name: ')
    device = input()

    par = london_co[device].keys()

    print('Input device param ({}): '.format(', '.join(london_co[device].keys())))
    param = input()
    ppp = []
    ppp.append(param)
    pp = set(par) & set(ppp)
    pppp = []
    pppp.append('Not in list')
    pv = list(pp)
    pv.append('ip')
    pppp.append(int(bool(pp)) * london_co[device][pv[0]])
    print(pppp[int(bool(pp))])


def task5_1d():
    london_co = {
        "r1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.1"
        },
        "r2": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "4451",
            "ios": "15.4",
            "ip": "10.255.0.2"
        },
        "sw1": {
            "location": "21 New Globe Walk",
            "vendor": "Cisco",
            "model": "3850",
            "ios": "3.6.XE",
            "ip": "10.255.0.101",
            "vlans": "10,20,30",
            "routing": True
        }
    }

    print('Input device name: ')
    device = input()

    par = london_co[device.lower()].keys()

    print('Input device param ({}): '.format(', '.join(london_co[device].keys())))
    param = input()
    ppp = []
    ppp.append(param.lower())
    pp = set(par) & set(ppp)
    pppp = []
    pppp.append('Not in list')
    pv = list(pp)
    pv.append('ip')
    pppp.append(int(bool(pp)) * london_co[device][pv[0]])
    print(pppp[int(bool(pp))])


def task5_2():
    print('input ip with mask (aaa.bbb.ccc.ddd/qq): ')
    ipwm = str(input())

    ip = ipwm[0:ipwm.find('/')]
    a = int(ip[0:ip.find('.')])
    b = int(ip[ip.find('.') + 1:ip.find('.', ip.find('.') + 1)])
    c = int(ip[ip.rfind('.', 0, ip.rfind('.') - 1) + 1:ip.rfind('.')])
    d = int(ip[ip.rfind('.') + 1::])
    mask = int(ipwm[ipwm.find('/') + 1::])
    print('ip:')
    print('{:8}  {:8}  {:8}  {:8}'.format(a, b, c, d))
    print('{:08b}  {:08b}  {:08b}  {:08b}'.format(a, b, c, d))

    print('mask:')
    print('/' + str(mask))
    mmask = '1' * mask + '0' * (32 - mask)
    ma = int(mmask[0:8], 2)
    mb = int(mmask[8:16], 2)
    mc = int(mmask[16:24], 2)
    md = int(mmask[24:32], 2)
    print(mmask[0:7])
    print('{:8}  {:8}  {:8}  {:8}'.format(ma, mb, mc, md))
    print('{:08b}  {:08b}  {:08b}  {:08b}'.format(ma, mb, mc, md))


def task5_2a():
    print('input ip with mask (aaa.bbb.ccc.ddd/qq): ')
    ipwm = str(input())

    ip = ipwm[0:ipwm.find('/')]
    a = int(ip[0:ip.find('.')])
    b = int(ip[ip.find('.') + 1:ip.find('.', ip.find('.') + 1)])
    c = int(ip[ip.rfind('.', 0, ip.rfind('.') - 1) + 1:ip.rfind('.')])
    d = int(ip[ip.rfind('.') + 1::])

    mask = int(ipwm[ipwm.find('/') + 1::])

    mmask = '1' * mask + '0' * (32 - mask)
    ma = int(mmask[0:8], 2)
    mb = int(mmask[8:16], 2)
    mc = int(mmask[16:24], 2)
    md = int(mmask[24:32], 2)

    p = '{:08b}{:08b}{:08b}{:08b}'.format(a, b, c, d)
    mp = '{:08b}{:08b}{:08b}{:08b}'.format(ma, mb, mc, md)

    np = p[0:mask] + '0' * (32 - mask)

    nma = int(np[0:8], 2)
    nmb = int(np[8:16], 2)
    nmc = int(np[16:24], 2)
    nmd = int(np[24:32], 2)

    print('network:')
    print('{:8}  {:8}  {:8}  {:8}'.format(nma, nmb, nmc, nmd))
    print('{:08b}  {:08b}  {:08b}  {:08b}'.format(nma, nmb, nmc, nmd))

    print('mask:')
    print('/' + str(mask))

    print('{:8}  {:8}  {:8}  {:8}'.format(ma, mb, mc, md))
    print('{:08b}  {:08b}  {:08b}  {:08b}'.format(ma, mb, mc, md))


def task5_3():
    access_template = [
        "switchport mode access", "switchport access vlan {}",
        "switchport nonegotiate", "spanning-tree portfast",
        "spanning-tree bpduguard enable"
    ]

    trunk_template = [
        "switchport trunk encapsulation dot1q", "switchport mode trunk",
        "switchport trunk allowed vlan {}"
    ]

    d = {'access': access_template,
         'trunk': trunk_template}

    q = {'access': 'Введите номер VLAN: ',
         'trunk': 'Введите разрешенные VLANы: '}

    print('Введите режим работы интерфейса (access/trunk): ')
    work = str(input())
    print('Введите тип и номер интерфейса: ')
    type = str(input())
    print(q[work])
    vlan = str(input())

    print('Interface {}'.format(type))

    print('\n'.join(d[work]).format(vlan))

def main():
    task5_3()

if __name__ == "__main__":
    main()

