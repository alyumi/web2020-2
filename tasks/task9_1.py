access_mode_template = [
    "switchport mode access", "switchport access vlan",
    "switchport nonegotiate", "spanning-tree portfast",
    "spanning-tree bpduguard enable"
]

access_config = {
    "FastEthernet0/12": 10,
    "FastEthernet0/14": 11,
    "FastEthernet0/16": 17
}


def generate_access_config(intf_vlan_mapping, access_template):
    """
    intf_vlan_mapping - словарь с соответствием интерфейс-VLAN такого вида:
        {"FastEthernet0/12": 10,
         "FastEthernet0/14": 11,
         "FastEthernet0/16": 17}
    access_template - список команд для порта в режиме access

    Возвращает список всех портов в режиме access с конфигурацией на основе шаблона
    """

    for elem in intf_vlan_mapping:
        print('interface {}'.format(elem))
        for template in access_template:
            if template.split()[-1] == 'vlan':
                print(template, intf_vlan_mapping.get(elem))
            else:
                print(template)
        print('\n')



def main():
    generate_access_config(access_config, access_mode_template)

if __name__ == "__main__":
    main()

