from sys import argv

ipwm = argv[1]

ip = ipwm[0:ipwm.find('/')]
a = int(ip[0:ip.find('.')])
b = int(ip[ip.find('.') + 1:ip.find('.', ip.find('.') + 1)])
c = int(ip[ip.rfind('.', 0, ip.rfind('.') - 1) + 1:ip.rfind('.')])
d = int(ip[ip.rfind('.') + 1::])


mask = int(ipwm[ipwm.find('/') + 1::])

mmask = '1' * mask + '0' * (32 - mask)
ma = int(mmask[0:8], 2)
mb = int(mmask[8:16], 2)
mc = int(mmask[16:24], 2)
md = int(mmask[24:32], 2)

p = '{:08b}{:08b}{:08b}{:08b}'.format(a, b, c, d)
mp = '{:08b}{:08b}{:08b}{:08b}'.format(ma, mb, mc, md)

np = p[0:mask] + '0' * (32 - mask)

nma = int(np[0:8], 2)
nmb = int(np[8:16], 2)
nmc = int(np[16:24], 2)
nmd = int(np[24:32], 2)

print('network:')
print('{:8}  {:8}  {:8}  {:8}'.format(nma, nmb, nmc, nmd))
print('{:08b}  {:08b}  {:08b}  {:08b}'.format(nma, nmb, nmc, nmd))

print('mask:')
print('/' + str(mask))

print('{:8}  {:8}  {:8}  {:8}'.format(ma, mb, mc, md))
print('{:08b}  {:08b}  {:08b}  {:08b}'.format(ma, mb, mc, md))